﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronBarCode;
using ZXing;
using System.Drawing;
namespace BarcodeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var rand = new Random();

            int numberOfColumns = 12;
            int numberOfRows = 8;
            double wellSpacingMM = 9.0;

            double barcodeSizeMM = 4.0;
            double tubeSizeMM = 7.0;
            double imageWidthMM = 150.0;

            int cameraSensorWidthPixels = 640 * 4;
            int cameraSensorHeightPixels = 480 * 4;

            double pixelsPerMM = (double)cameraSensorWidthPixels / imageWidthMM;

            double imageHeightMM = (double)cameraSensorHeightPixels / pixelsPerMM;

            int barcodeSizePixels = (int)(barcodeSizeMM * pixelsPerMM);
            int barcodeSpacingPixels = (int)(wellSpacingMM * pixelsPerMM);
            int tubeSizePixels = (int)(tubeSizeMM * pixelsPerMM);

            double barcodeXOffsetMM = imageWidthMM / 2 - ((double)numberOfColumns / 2 + 0.5) * wellSpacingMM;
            int barcodeXOffsetPixels = (int)(barcodeXOffsetMM * pixelsPerMM);


            double barcodeYOffsetMM = imageHeightMM / 2 - ((double)numberOfRows / 2 + 0.5) * wellSpacingMM;
            int barcodeYOffsetPixels = (int)(barcodeYOffsetMM * pixelsPerMM);

            var testImage = new Bitmap(cameraSensorWidthPixels, cameraSensorHeightPixels);
            var g = Graphics.FromImage(testImage);
            g.Clear(Color.Gray);

            var writtenBarcodes = new Dictionary<Tuple<int, int>, string>();
            for (int x = 0; x < numberOfColumns; ++x)
            {
                for (int y = 0; y < numberOfRows; ++y)
                {
                    var barcodeString = x + "," + y;
                    writtenBarcodes.Add(new Tuple<int, int>(x, y), barcodeString);
                    int xOffset = (barcodeXOffsetPixels + x * barcodeSpacingPixels);
                    int yOffset = (barcodeYOffsetPixels + y * barcodeSpacingPixels);

                    g.FillEllipse(Brushes.White, xOffset - tubeSizePixels / 2, yOffset - tubeSizePixels / 2, tubeSizePixels, tubeSizePixels);

                    var barcode = IronBarCode.BarcodeWriter.CreateBarcode(barcodeString, BarcodeEncoding.DataMatrix, barcodeSizePixels, barcodeSizePixels);
                    g.TranslateTransform(-barcodeSizePixels / 2, -barcodeSizePixels / 2);
                    g.RotateTransform((float)(rand.NextDouble() * 360), System.Drawing.Drawing2D.MatrixOrder.Append);
                    g.TranslateTransform(xOffset, yOffset, System.Drawing.Drawing2D.MatrixOrder.Append);
                    g.DrawImage(barcode.Image, 0, 0);

                    g.ResetTransform();
                }
            }

            testImage.Save(@"C:\Users\NickHowells\Desktop\barcodetest\bcr" + DateTime.Now.Ticks + ".bmp");

            var windowImage = new Bitmap(barcodeSpacingPixels, barcodeSpacingPixels);
            var windowImageRotations = new List<Bitmap>();
            var windowImageRotationGraphics = new List<Graphics>();

            var rotations = new float[] { 0, 45, 180, 270 };
            foreach (var r in rotations)
            {
                var rimg = new Bitmap(windowImage.Width, windowImage.Height);
                windowImageRotations.Add(rimg);

                windowImageRotationGraphics.Add(Graphics.FromImage(rimg));
            }

            var windowGraphics = Graphics.FromImage(windowImage);

            for (int x = 0; x < numberOfColumns; ++x)
            {
                for (int y = 0; y < numberOfRows; ++y)
                {
                    var expectedBarcodeString = x + "," + y;

                    int xOffset = (barcodeXOffsetPixels + x * barcodeSpacingPixels);
                    int yOffset = (barcodeYOffsetPixels + y * barcodeSpacingPixels);

                    windowGraphics.DrawImage(testImage, new Rectangle(0, 0, windowImage.Width, windowImage.Height), new Rectangle(xOffset - windowImage.Width / 2, yOffset - windowImage.Height / 2, windowImage.Width, windowImage.Height), GraphicsUnit.Pixel);
                    windowImage.Save(@"C:\Users\NickHowells\Desktop\barcodetest\bcr" + DateTime.Now.Ticks + ".bmp");

                    var resultString = "";
                    for (int r = 0; r < rotations.Length; ++r)
                    {
                        var gw = windowImageRotationGraphics[r];

                        gw.TranslateTransform(-windowImage.Width / 2, -windowImage.Height / 2);
                        gw.RotateTransform(rotations[r], System.Drawing.Drawing2D.MatrixOrder.Append);
                        gw.TranslateTransform(windowImage.Width / 2, windowImage.Height / 2, System.Drawing.Drawing2D.MatrixOrder.Append);
                        gw.DrawImage(windowImage, 0, 0);
                        gw.ResetTransform();
                    }
                    for (int r = 0; r < rotations.Length; ++r)
                    {
                        //windowImageRotations[r].Save(@"C:\Users\NickHowells\Desktop\barcodetest\bcr" + DateTime.Now.Ticks + ".bmp");

                        var irongBarcodeResult = IronBarCode.BarcodeReader.ReadASingleBarcode(windowImageRotations[r], BarcodeEncoding.DataMatrix, IronBarCode.BarcodeReader.BarcodeRotationCorrection.Low);

                        if(irongBarcodeResult != null && (irongBarcodeResult.Text == expectedBarcodeString))
                        {
                            resultString += "SUCCESS ";                           
                        }
                        else
                        {
                            resultString += "FAIL ";
                        }
                    }
                    resultString += "||";
                    for (int r = 0; r < rotations.Length; ++r)
                    {

                        var luminanceSource = new ZXing.BitmapLuminanceSource(windowImageRotations[r]);
                        var zxingReader = new ZXing.BarcodeReader(
                            new ZXing.Datamatrix.DataMatrixReader(),
                            bm => new ZXing.BitmapLuminanceSource(windowImageRotations[r]),
                            ls => new ZXing.Common.HybridBinarizer(luminanceSource))
                        {
                            AutoRotate = true,
                            Options =
                            {
                                PossibleFormats = new List<BarcodeFormat> {BarcodeFormat.DATA_MATRIX},
                                TryHarder = true
                            }
                        };

                        var zxingResult = zxingReader.Decode(windowImageRotations[r]);


                        if (zxingResult != null && (zxingResult.Text == expectedBarcodeString))
                        {
                            resultString += "SUCCESS ";
                        }
                        else
                        {
                            resultString += "FAIL ";
                        }
                    }

                    System.Diagnostics.Debug.WriteLine(resultString);

                }
            }

        }
    }
}
